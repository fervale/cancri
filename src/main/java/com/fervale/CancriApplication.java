package com.fervale;

import com.fervale.domain.Role;
import com.fervale.services.AccountService;
import com.fervale.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CancriApplication implements CommandLineRunner {

	@Autowired
	private UserService userService;

	@Autowired
	private AccountService accountService;

	public static void main(String[] args) {
		SpringApplication.run(CancriApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		userService.createUser("user1", "123");
		userService.createUser("user2", "456");
		userService.createUser("user3", "789");
		userService.createUser("user4", "789");

		accountService.createAccount("user1", Role.ADMIN);
		accountService.createAccount("user2", Role.SELLER);

		accountService.lookup().forEach(account -> System.out.println(account));
		userService.lookup().forEach(user -> System.out.println(user));
	}
}
