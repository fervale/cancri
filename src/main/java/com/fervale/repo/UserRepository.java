package com.fervale.repo;

import com.fervale.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by vkhusnulina on 18/03/17.
 */
public interface UserRepository extends PagingAndSortingRepository<User, Integer> {

    User findByUsername(@Param("username") String username);

    Page<User> findByPassword(@Param("pass") String password, Pageable pageable);
}
