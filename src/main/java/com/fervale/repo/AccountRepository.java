package com.fervale.repo;

import com.fervale.domain.Account;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by vkhusnulina on 18/03/17.
 */
public interface AccountRepository extends CrudRepository<Account, Integer> {
}
