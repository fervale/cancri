package com.fervale.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by vkhusnulina on 18/03/17.
 */
@Entity
public class User implements Serializable{

    @Id
    @GeneratedValue
    private Integer id;

    @Column(length = 50, unique = true)
    private String username;

    @Column
    private String password;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    protected User(){}

    @Override
    public String toString(){
        return this.id + " " + this.username;
    }

}
