package com.fervale.domain;

import javax.persistence.*;

/**
 * Created by vkhusnulina on 18/03/17.
 */
@Entity
public class Account {

    @Id
    @GeneratedValue
    private Integer id;


    @OneToOne
    private User user;

    @Column
    private Role role;


    public User getUser() {
        return user;
    }

    public Role getRole() {
        return role;
    }
    public Account(User user, Role role) {
        this.user = user;
        this.role = role;
    }

    protected Account(){}

    @Override
    public String toString(){
        return this.id + " " + this.user + " " + this.role;
    }
}
