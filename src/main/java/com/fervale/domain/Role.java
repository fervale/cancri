package com.fervale.domain;

/**
 * Created by vkhusnulina on 18/03/17.
 */
public enum Role {
    ADMIN, SELLER
}
