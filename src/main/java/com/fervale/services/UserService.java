package com.fervale.services;

import com.fervale.domain.User;
import com.fervale.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;

/**
 * Created by vkhusnulina on 18/03/17.
 */
@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void createUser(String username, String password){
        userRepository.findByUsername(username);
        if(userRepository.findByUsername(username) == null){
            userRepository.save(new User(username, password));
        } else {
            throw new RuntimeException("Username already exists: " + username);
        }
    }

    public Iterable<User> lookup(){
        return userRepository.findAll();
    }
}
