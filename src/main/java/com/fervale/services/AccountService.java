package com.fervale.services;

import com.fervale.domain.Account;
import com.fervale.domain.Role;
import com.fervale.domain.User;
import com.fervale.repo.AccountRepository;
import com.fervale.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by vkhusnulina on 18/03/17.
 */
@Service
public class AccountService {

    private AccountRepository accountRepository;
    private UserRepository userRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository, UserRepository userRepository) {
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
    }

    public void createAccount(String username, Role role){
        User user = userRepository.findByUsername(username);
        if(user != null){
            accountRepository.save(new Account(user, role));
        }
    }

    public Iterable<Account> lookup(){
        return accountRepository.findAll();
    }
}
